
default['data_ec2']['users'] = %w(steve michelle marketing-user)
default['data_ec2']['sudousers'] = %w(steve michelle)
default['data_ec2']['marketingusers'] = %w(steve michelle marketing-user)
