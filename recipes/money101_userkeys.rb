
file '/data/home/michelle/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt9WaWOWvobsfKsSraKqkSRdHOtPh4ZmRxzoN34UPvDNMUZL6xG/tOeGPLxcz80+aMhG+sSfuTmPUTJa2t3ZVpgieHr032ITpf1k5g8ESjgYGwzZLGZvlU7Y7QMrDxVQhTuESqgfKQVQ/5R0xtYfLlhYodEuQXOOyq/QXpHMP0pBAmbefACR886FuJJz8MhnZDUDM2dH2qPiRwSGHhEnJw2s6z1JxvQsMuevd53f39SQ/48AVdJtpPBn7ki9cHtZ5jeglAiDYYMG13UQi+F3jB4lP5L/xluSxIi0BX4yBzO7tcwh9ZbpFwB+/f09UMEuPADbyNpiX1Mg5tE7VGvPm9 michelle@Michelles-MacBook-Pro.local'
  mode '600'
  owner 'michelle'
  group 'michelle'
end

file '/data/home/steve/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDm5B4ce+mGbe2K/6BxoLJs24PpARV6UtP1IBtvE/0OoQCyvX34pimpZq/ZL7rTy5LeKkGWNDwcC8AQsIptK5xHjCcAEj6FD5VvdRYY0r6GjX5CX1c/zd0X3Q/JTZpCxjFHiUMRj/HRPenk8QjFsdjKHHI4kesR70DeLn30kfEKQuTndgtw9ZBN18dsfiWojWW/IHmWb0OHDDZ4gPhdtMy20pJNHWeDCIdMuQUyD7Gh2uKwgpZG+w1MSxkVfRl93a9jFoo7DFyZA+AB/eCp4fwrvwO0AvmqRQr7FxxJ6tNtGc/iJnSxxR2jcdOy8gOpN3eNr/tXi8IwC6On3QQyrFqb steve@steves-MacBook-Pro.local'
  mode '600'
  owner 'steve'
  group 'steve'
end

file '/data/home/marketing-user/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt9WaWOWvobsfKsSraKqkSRdHOtPh4ZmRxzoN34UPvDNMUZL6xG/tOeGPLxcz80+aMhG+sSfuTmPUTJa2t3ZVpgieHr032ITpf1k5g8ESjgYGwzZLGZvlU7Y7QMrDxVQhTuESqgfKQVQ/5R0xtYfLlhYodEuQXOOyq/QXpHMP0pBAmbefACR886FuJJz8MhnZDUDM2dH2qPiRwSGHhEnJw2s6z1JxvQsMuevd53f39SQ/48AVdJtpPBn7ki9cHtZ5jeglAiDYYMG13UQi+F3jB4lP5L/xluSxIi0BX4yBzO7tcwh9ZbpFwB+/f09UMEuPADbyNpiX1Mg5tE7VGvPm9 michelle@Michelles-MacBook-Pro.local
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDm5B4ce+mGbe2K/6BxoLJs24PpARV6UtP1IBtvE/0OoQCyvX34pimpZq/ZL7rTy5LeKkGWNDwcC8AQsIptK5xHjCcAEj6FD5VvdRYY0r6GjX5CX1c/zd0X3Q/JTZpCxjFHiUMRj/HRPenk8QjFsdjKHHI4kesR70DeLn30kfEKQuTndgtw9ZBN18dsfiWojWW/IHmWb0OHDDZ4gPhdtMy20pJNHWeDCIdMuQUyD7Gh2uKwgpZG+w1MSxkVfRl93a9jFoo7DFyZA+AB/eCp4fwrvwO0AvmqRQr7FxxJ6tNtGc/iJnSxxR2jcdOy8gOpN3eNr/tXi8IwC6On3QQyrFqb steve@steves-MacBook-Pro.local
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFblQntVAwuU+2JbiPurA41tCRaSjZ3y5HxNkwwo34MO ed25519-key-20190211
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC18bwDCjiS4MjETJcT/StQpcyW92GUlrgjXvN6gSBwY9bWV/NLaHMR8WKdpyZ9/gycijEQHTXF69w+GmiMEie+ou/A0umiv9MgsM7OUOMraZg6I0jcVQR/7T0nrjtDZCXDbTaxZ0UU1lpFWp325V5qxv0qQvEPrkHHtP73Su7rIkG1DKag7j1esV9KEtCvKj5v+58ZIiY6zWy165EnGVAxdkGCQCLMLd2YqtqWbuw+O5bGFPbjCHXnCILdMPHLq5hlBTcpDxSjuxv4V61QVGMYKW8igWNanRKhMSEswQbxErq68/uH+zBK6PmmsBshT4ZSVDe/GoVtRzjMNwm3SygZ m101-ryan@Ryan-MacBook-Pro.local'
  mode '600'
  owner 'marketing-user'
  group 'marketing-user'
end