
file '/data/home/michelle/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt9WaWOWvobsfKsSraKqkSRdHOtPh4ZmRxzoN34UPvDNMUZL6xG/tOeGPLxcz80+aMhG+sSfuTmPUTJa2t3ZVpgieHr032ITpf1k5g8ESjgYGwzZLGZvlU7Y7QMrDxVQhTuESqgfKQVQ/5R0xtYfLlhYodEuQXOOyq/QXpHMP0pBAmbefACR886FuJJz8MhnZDUDM2dH2qPiRwSGHhEnJw2s6z1JxvQsMuevd53f39SQ/48AVdJtpPBn7ki9cHtZ5jeglAiDYYMG13UQi+F3jB4lP5L/xluSxIi0BX4yBzO7tcwh9ZbpFwB+/f09UMEuPADbyNpiX1Mg5tE7VGvPm9 michelle@Michelles-MacBook-Pro.local'
  mode '600'
  owner 'michelle'
  group 'michelle'
end

file '/data/home/steve/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDm5B4ce+mGbe2K/6BxoLJs24PpARV6UtP1IBtvE/0OoQCyvX34pimpZq/ZL7rTy5LeKkGWNDwcC8AQsIptK5xHjCcAEj6FD5VvdRYY0r6GjX5CX1c/zd0X3Q/JTZpCxjFHiUMRj/HRPenk8QjFsdjKHHI4kesR70DeLn30kfEKQuTndgtw9ZBN18dsfiWojWW/IHmWb0OHDDZ4gPhdtMy20pJNHWeDCIdMuQUyD7Gh2uKwgpZG+w1MSxkVfRl93a9jFoo7DFyZA+AB/eCp4fwrvwO0AvmqRQr7FxxJ6tNtGc/iJnSxxR2jcdOy8gOpN3eNr/tXi8IwC6On3QQyrFqb steve@steves-MacBook-Pro.local'
  mode '600'
  owner 'steve'
  group 'steve'
end

file '/data/home/marketing-user/.ssh/authorized_keys' do
  content 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt9WaWOWvobsfKsSraKqkSRdHOtPh4ZmRxzoN34UPvDNMUZL6xG/tOeGPLxcz80+aMhG+sSfuTmPUTJa2t3ZVpgieHr032ITpf1k5g8ESjgYGwzZLGZvlU7Y7QMrDxVQhTuESqgfKQVQ/5R0xtYfLlhYodEuQXOOyq/QXpHMP0pBAmbefACR886FuJJz8MhnZDUDM2dH2qPiRwSGHhEnJw2s6z1JxvQsMuevd53f39SQ/48AVdJtpPBn7ki9cHtZ5jeglAiDYYMG13UQi+F3jB4lP5L/xluSxIi0BX4yBzO7tcwh9ZbpFwB+/f09UMEuPADbyNpiX1Mg5tE7VGvPm9 michelle@Michelles-MacBook-Pro.local
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDm5B4ce+mGbe2K/6BxoLJs24PpARV6UtP1IBtvE/0OoQCyvX34pimpZq/ZL7rTy5LeKkGWNDwcC8AQsIptK5xHjCcAEj6FD5VvdRYY0r6GjX5CX1c/zd0X3Q/JTZpCxjFHiUMRj/HRPenk8QjFsdjKHHI4kesR70DeLn30kfEKQuTndgtw9ZBN18dsfiWojWW/IHmWb0OHDDZ4gPhdtMy20pJNHWeDCIdMuQUyD7Gh2uKwgpZG+w1MSxkVfRl93a9jFoo7DFyZA+AB/eCp4fwrvwO0AvmqRQr7FxxJ6tNtGc/iJnSxxR2jcdOy8gOpN3eNr/tXi8IwC6On3QQyrFqb steve@steves-MacBook-Pro.local
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDd+xJf6SQICg/GmSXoKX5uQMP09LMHS6Bc/eAxWbPtL+f7fUFOf5+CSZGbJEt1PaNdJJN/lh978tr4NPNWV80D3pF5/2NWwU4PcMx6BEnDnB2qYC3ni5Ag9KU4GRN4Fz2qT6rtgjdU2d4c/ScTXMlQnIzrC+3Pps0gRaujVNVIVo66L11niTeDWr9EG6Z96JDAwRde3IQzUU276ahfvFs7gcxGq0epwbXsxJSq+LkOUXT6/yd9eHJdZPagBvGpynq5APCTKpQjDBtlYGnAfz5OvcuS1peA8udUOg4yPCkea+4X4xpW8CJllP6s1HtIbryQgN/nrVwkj5oTTlhMnC//
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFblQntVAwuU+2JbiPurA41tCRaSjZ3y5HxNkwwo34MO ed25519-key-20190211
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9htznCCDBgc27sRH4K7ziovlEPvLHsyQOyXf+zkoc3LESp19/7Owf8jddHXKGDbjCaOp/BGDIwk4kad4pveezEhDg2dXo71kx6W+PCoIQbUziEobZmXvseInnToeNHV7IwuX79fUakqk14VmJ+0kV3THIWbjply1EB3Xy+txFOPZ917zwVJFlnjE9Fv7ZSeqgh/rtBVVl/ei19zJ62HB0mzStfM6mFVtJOPthTmtrfjavyTMqxlq4teb/HEwoXho1urY4R1MbSwhcJhQricGCb7EFRORZfZWDe9Uys5aH0PmQgERtcYDLYfrqab5jtX19iuIntXW/cibo61ALkl5J
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzLAsSRTEt5f+IxkmPpKGIBwerTu/W/QTGsuhsSFQdn2ZnDBC3kHlC/yu4mBq6YUODRoVXnwMwT8AHr6nuT4JELDCe+wKcMQiM807R1qT2mo54vzuiKcN61mTDmiqjB8bwuF3E4WmKrqZlRDfKxcjKejtgz1RMdL+MVhCkpIaVteKjTNsa9rReY4XWrDIso9eqHeg4kdi3+awZSZkl1TyFNOTUu2ShErKZmay0mvEIxegEt3WQoWd/D+q4EFf/KwoQbiTUTrzruqjviNl7VNSslsGUqcoPCuyXE/8MUTZBbqvXjVxkMDieZeTzbfD82Oa+4vVV8Mw6pmFera6Lkifh ramesh@DESKTOP-IFPLHS3
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiAe7BGCu01Ik8yQRZDIpYjNzB0YKOb2rFjw271ZSPxuaO4ZU5TT8XXwUBH7zWGUoLE7HYmqkDTUuOQBgOeFAr5Kpw0wk5DhD9PVuUqASJUS6CY0Q1coJ5kULLFZeqELY9nDx52fVj5qZa6GPClaqcZpMxsRgvDA3IP1c+8wPXX4qQuXcRGAeNw7EWtW1Ve9ThTx7r+EwerCHnKlrJyPQJ42L3I4MPMeDtCFTaD5F7+dfuUbxqYbqZhGddR77FysO0a2paViBjs/Vcd2dAXir/vTF5kGXI4UxNELsYNO8SCqG2Oo0n3mLmzcfZZFN5xr660aLPa/cJGWu9lNcKr2BN
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBu9P+LDVAT9wBmVL/v6GtvxYojN3ykxqkrYYeezPsjgD4b0kzcXBTdsFvespGQcuHnMUB04qPa40+buLXZt4sMux+f4jWVTXi/kYJ4AMEEu5so0CAABIi52wEfcb6WdS/qcNnBniy5A/gaNQzZbNbB/KU8Lvti2RTfFlKRBSZn4GyoN/4SODTjeZ2MF6v0TYOUa4SjePdDj9+5FRT4iT99Im1DB/rccOyfdcURSP8a9rT1bqK06Kg2bAEUE/s2RTpd/6Fu8nyvQA9zWQRZ3SCVkIx6Rk3xP51nQgqi9WzeGZHG9EoYhkQGEf1/Rg0YDoxL26OMIx1rO+pzuDgEYJz
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6ea9GTIZ2mnfAiFZmU3WoxSkTMBIvu02zkVK23y7yLWE5e8uihE+nrWP/MfQN2y5oj+HmSNepPrpeibpva0H0InSL9aosfaqdAVq6x22//lakLfXLrq6ybD4oEu1MA5Td2m+CvG3bzLHwT6zN2Q5w2fThR7cx3o6uRee5rVKuYaYQ61hFVc23/iTifFfgGBG4gyEzvv5KprJiHdazg6vfl5uQNNeJqRiPEzxO0/NwDlJDX5Msc4dAlWuN2oyVywVHhP4CXse6YJgN/SC98EoMUPZLulHuaXKz3m968yuDazgDR7aHgCm1j022qqZjgCer5lGAgbsEHrxyj6/npKDb comparehero@Compareheros-MacBook-Pro.local'
  mode '600'
  owner 'marketing-user'
  group 'marketing-user'
end