#
# Cookbook:: data_ec2
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

apt_repository 'openjdk-11-jdk' do
  uri          'ppa:openjdk-r/ppa'
end

apt_repository 'python3.7-dev' do
  uri          'ppa:deadsnakes/ppa'
end

package %w(unzip libpq-dev gcc musl-dev python3.7-dev libmysqlclient-dev openjdk-11-jdk python3-venv python3.7-venv)

alternatives 'python install 3' do
  link_name 'python3'
  path '/usr/bin/python3.5'
  priority 100
  action :install
end

alternatives 'python install 3' do
  link_name 'python3'
  path '/usr/bin/python3.7'
  priority 200
  action :install
end

group 'marketing' do
  action :create
end

sudo 'nopass_sudogroup' do
  groups 'sudo'
  nopasswd true
end

node['data_ec2']['users'].each do |username|
  directory "/data/home/#{username}/.ssh" do
    recursive true
    mode '700'
  end
end

node['data_ec2']['users'].each do |username|
  user username do
    shell '/bin/bash'
    home "/data/home/#{username}"
    manage_home true
    action :create
  end
end

node['data_ec2']['users'].each do |username|
  directory "/data/home/#{username}/.ssh" do
    group "#{username}"
    owner "#{username}"
    recursive true
  end
end

node['data_ec2']['users'].each do |username|
  directory "/data/home/#{username}" do
    group "#{username}"
    owner "#{username}"
    mode '755'
  end
end

group 'add sudo users to sudo group' do
  group_name 'sudo'
  action :modify
  members node['data_ec2']['sudousers']
  append true
end

group 'add marketingusers to marketing group' do
  group_name 'marketing'
  action :modify
  members node['data_ec2']['marketingusers']
  append true
end

directory '/data/marketing' do
  group 'marketing'
  owner 'marketing-user'
  mode '775'
  recursive true
end