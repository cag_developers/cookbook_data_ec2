# InSpec test for recipe data_ec2::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

unless os.windows?
  # This is an example test, replace with your own test.
  describe user('root'), :skip do
    it { should exist }
  end
end

# This is an example test, replace it with your own test.
describe port(80), :skip do
  it { should_not be_listening }
end

describe package('python3.7') do
  it { should be_installed }
end

describe package('openjdk-11-jdk') do
  it { should be_installed }
end

describe apt('ppa:deadsnakes/ppa') do
  it { should exist }
  it { should be_enabled }
end

describe apt('ppa:openjdk-r/ppa') do
  it { should exist }
  it { should be_enabled }
end

describe group('marketing') do
  it { should exist }
  its('members') { should include ['steve', 'michelle'] }
end

describe user('steve') do
  it { should exist }
  its('groups') { should eq ['steve', 'sudo', 'marketing']}
  its('home') { should eq '/data/home/steve' }
  its('shell') { should eq '/bin/bash' }
end
