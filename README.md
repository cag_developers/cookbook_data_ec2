# data_ec2

This Cookbook is to do package & user managment for the Data EC2 instances.

Once the Instance has been created in AWS

1. Check and add packages(if required) that need to be installed in the recipes/default.rb file
2. Add the required users in the attributes/default.rb file
3. Add the user public ssh key to the recipes/{environment}_userkeys.rb file
4. Run converge on the Instance:
    1. `chef-run -i id_rsa.pem ubuntu@data.priv.dev.cag.systems data_ec2/recipes/default.rb`
    2. `chef-run -i id_rsa.pem ubuntu@data.priv.dev.cag.systems data_ec2/recipes/dev_userkeys`
5. Check that the chef converge was successful, to troubleshoot you can look at the logs on your local machine
    ~/.chef-workstation/logs/default.log
    
# SSH keys for running chef
Keys can be retrieved from tf-cag repo under `**/data-ec2-prod` after running `terragrunt apply`. see the [module](https://bitbucket.org/cag_developers/tf-cag/src/master/modules/composed/data-ec2/)

# Adding SSH keys to authorized_keys
See `recipes/*_userskeys.rb`. As a good practice, always add the names of the person to the [ssh keys as comments](https://serverfault.com/questions/442933/add-comment-to-existing-ssh-public-key), this would making tracking down keys slightly easier.

# Set up
## Accepting chef licenses
If you have not accepted the licenses, chef-run commands would fail. You just have to run this command
[Chef licenses docs](https://docs.chef.io/chef_license_accept/)\

```
chef-run --chef-license accept
```

## [Installing Chef](https://docs.chef.io/workstation/install_workstation/#installation)
### MacOS

```
brew cask install chef-workstation
```
